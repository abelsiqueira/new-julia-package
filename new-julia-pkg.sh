#/bin/bash

# Is lice installed?
command -v lice gplv3 >/dev/null 2>&1 || {
  echo "Need lice installed"
  exit 1
}

# Pkg name was passed?
[ $# -lt 1 ] && echo "Need package name" && exit 1

name=${@//.jl/}
pkg=""
for arg in $name
do
  pkg=$pkg${arg^}
done

echo $pkg | grep "\." >/dev/null && {
  echo "File has '.' (dot). Forbidden by Julia"
  exit 1
}

mkdir -p ${pkg}.jl/{src,test}
# Later check if it's empty
cd ${pkg}.jl

lice gpl3 | awk '{print "> "$0}' | sed 's/\s\+$//g' > LICENSE.md
cat > README.md << EOF
# ${pkg}
EOF
cat > src/${pkg}.jl << EOF
module ${pkg}

function ok ()
  println("Ok")
end

end
EOF

cat > test/runtests.jl << EOF
using ${pkg}

${pkg}.ok()
EOF

cat > .travis.yml << EOF
language: julia
julia:
  - release
  - nightly
notifications:
  email: false
script:
  - if [[ -a .git/shallow ]]; then git fetch --unshallow; fi
  - julia --check-bounds=yes -e 'Pkg.clone(pwd()); Pkg.build("${pkg}"); Pkg.test("${pkg}"; coverage=true)'
after_success:
  - julia -e 'cd(Pkg.dir("${pkg}")); Pkg.add("Coverage"); using Coverage; Coveralls.submit(Coveralls.process_folder())'
EOF

cat > Makefile << EOF
local:
	julia -E 'include("src/${pkg}.jl"); include("test/runtests.jl")'

# Will fail if you don't have Coverage installed (Pkg.add("Coverage") suffices)
cov:
	julia --code-coverage=user -E 'include("src/${pkg}.jl"); include("test/runtests.jl")'
	julia -E 'using Coverage; c, t = coverage_folder(); println("Cov = ", round(100*c/t,2))'

pkg:
	julia -E 'Pkg.rm("${pkg}"); Pkg.clone(pwd()); Pkg.build("${pkg}"); Pkg.test("${pkg}")'
EOF

echo "Makefile" > .gitignore

git init >/dev/null
git add .
git commit -m 'First commit' >/dev/null

# Testing
make local
make conv
make pkg
