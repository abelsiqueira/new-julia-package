# julia-new

This little package has the tools the create a simple working Travis package
folder with

  - LICENSE.md (With a default license)
  - README.md (Defaults to the package title)
  - src/YourPackage.jl (Creates a module, and a simple function)
  - test/runtests.jl ('use' the module and run the function)
  - .travis.yml
  - .git (Created by `git init`)
  - Makefile (For testing, not added on your package)

Every other file except `Makefile` is added to git and commit with the message
"First commit".

## Install

Download `install.sh` and run it. Alternatively, use the following command to
automatically download and run it.

    curl -sL https://gitlab.com/abelsiqueira/new-julia-package/raw/master/install.sh

Note that, currently, we require that you have
[lice](https://github.com/licenses/lice) installed, and `make` for testing.

## Usage

With the script installed, run

    new-julia-package my package name

to generate a `MyPackageName.jl` folder with the files indicated on top.
The script also tests the package with Makefile
(i) locally, (ii) with
[Coverage](https://github.com/IainNZ/Coverage.jl),
and (iii) installing the pkg to the julia folder.

## License

All code is licensed under GNU GPLv3.
