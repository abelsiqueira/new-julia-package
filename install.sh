#/bin/bash

script=new-julia-pkg
url=https://gitlab.com/abelsiqueira/new-julia-package/raw/master/${script}.sh

cd $(mktemp -d)
echo Downloading script $script
curl -s $url > $script
chmod a+x $script

sudo=""
if [ ! -w /usr/local/bin ]
then
  sudo="sudo"
  echo "Warning: you will be asked for your sudo password"
fi
$sudo cp $script /usr/local/bin
